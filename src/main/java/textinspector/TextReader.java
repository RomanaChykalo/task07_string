package textinspector;

import java.io.*;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TextReader {

    public List<String> readStrings() {
        File file = new File("text.txt");
        List<String> wordsList = new ArrayList<String>();

        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            String read;
            while ((read = bufferedReader.readLine()) != null) {
                wordsList.add(read);
            }
        } catch (
                FileNotFoundException e) {
            System.out.println("File not found.");
        } catch (
                IOException e) {
            System.out.println("I/O error.");
        }
        return wordsList;
    }

    public String deleteSequenceOfSpacesInText() {
        TextReader textReader = new TextReader();
        List<String> wordList = textReader.readStrings();
        String listString = "";
        for (String string : wordList) {
            listString += string;
        }
        String s = listString.replaceAll("\\s+", " ");
        return s;
    }

    public List<String> slitToTheSentence() {
        TextReader textReader = new TextReader();
        String textWithoutManySpaces = textReader.deleteSequenceOfSpacesInText();
        List<String> sentences = Arrays.asList(textWithoutManySpaces.split("\\.|\\?|\\!"));
        return sentences;
    }


    public void countWordsInAllText() {
        TextReader textReader = new TextReader();
        List<String> sentences = textReader.slitToTheSentence();
        Map<String, Integer> wordMap = new HashMap<>();

        for (String string : sentences) {
            Pattern pattern = Pattern.compile("\\s+");
            String[] words = pattern.split(string);
            for (String word : words) {
                if (wordMap.containsKey(word)) {
                    wordMap.put(word, (wordMap.get(word) + 1));
                } else {
                    wordMap.put(word, 1);
                }
            }
            for (Map.Entry<String, Integer> entry : wordMap.entrySet()) {
                if (entry.getValue() > 1) {
                    System.out.println(entry.getKey() + " => " + entry.getValue());
                }
            }

        }
    }

    public void fintAmountOfSentencesWithRepetitiveWords() {
        TextReader textReader = new TextReader();
        List<String> sentences = textReader.slitToTheSentence();
        int countOfSameWords = 0;
        for (String string : sentences) {
            List<String> words = Arrays.asList(string.toLowerCase().split("\\s+"));
            List<String> sortedWordsOfSentance = words.stream().sorted().collect(Collectors.toList());
            for (int i = 0; i < sortedWordsOfSentance.size() - 1; i++) {
                if (isSameWords(sortedWordsOfSentance, i, i + 1)) {
                    countOfSameWords += 1;
                    break;
                }
            }
        }
        System.out.print(countOfSameWords);
    }

    public boolean isSameWords(List<String> words, int i, int j) {
        return words.get(i).equalsIgnoreCase((words.get(j)));
    }

    public void sortAndPrintSentancesFromBigToLittle() {
        TextReader textReader = new TextReader();
        List<String> sentences = textReader.slitToTheSentence();
        Map<String, Integer> sentence = new TreeMap<>();
        for (String string : sentences) {
            int words = Arrays.asList(string.split("\\s+")).size();
            sentence.put(string, words);
        }
        for (Map.Entry<String, Integer> entry : sentence.entrySet()) {
            if (entry.getValue() > 1) {
                System.out.println(entry.getValue() + " => " + entry.getKey());
            }
        }
    }

    public Set<String> findUniqueWordFromFirstSentence() {
        TextReader textReader = new TextReader();
        List<String> sentences = textReader.slitToTheSentence();
        String firstSentence = sentences.get(0);
        String allSentenceAmongFirst = "";
        for (int i = 1; i < sentences.size(); i++) {
            allSentenceAmongFirst += sentences.get(i) + " ";
        }
        List<String> wordsOfFirstSentence = Arrays.asList(firstSentence.toLowerCase().split("\\s+"));
        List<String> wordsOfAllSentenceAmongFitst = Arrays.asList(allSentenceAmongFirst.toLowerCase().split("[\\s+]"));
        Set<String> different = new HashSet<>(wordsOfFirstSentence);
        different.removeAll(wordsOfAllSentenceAmongFitst);
        return different;

    }

    public void findSpecificLenghtWordsFromQuestionSentences(int length) {
        TextReader textReader = new TextReader();
        List<String> list = textReader.readStrings();
        String string = list.toString().toLowerCase().replace("?", "&");
        List<String> splittered = Arrays.asList(string.split("\\.|\\?|\\!"));
        List<String> questioningSentences = splittered.stream().filter(s -> s.contains("&")).collect(Collectors.toList());
        String[] wordsOfQuestionSentences = questioningSentences.toString().split(" ");
        Set<String> specificLengthWords = Stream.of(wordsOfQuestionSentences).filter(w -> w.length() == length).collect(Collectors.toSet());
        System.out.println(specificLengthWords);
    }

}