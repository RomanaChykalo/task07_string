package textinspector.menu;

import textinspector.TextReader;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.*;


public class App {
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    Locale locale;
    ResourceBundle bundle;

    public App() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        methodsMenu = new LinkedHashMap<>();

        methodsMenu.put("1", this::testStringUtils);
        methodsMenu.put("2", this::showUkraineInfo);
        methodsMenu.put("3", this::showEnglishInfo);
        methodsMenu.put("4", this::testRegEx);
        methodsMenu.put("5", this::infoUkraineCurrentTime);
        methodsMenu.put("6", this::infoEnglishCurrentTime);
    }

    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("6", bundle.getString("6"));
        menu.put("Q", bundle.getString("Q"));
    }

    public void infoEnglishCurrentTime() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("Menu", locale);
        System.out.println(bundle.getString("10"));
        String currentDate = DateFormat.getDateInstance(DateFormat.FULL, locale).format(new Date());
        String averageSalary = NumberFormat.getCurrencyInstance(locale).format(5000);
        System.out.println("Average salary in Great Britain is: " + averageSalary);
        System.out.println("Today is " + currentDate);
    }

    public void infoUkraineCurrentTime() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("Menu", locale);
        System.out.println(bundle.getString(String.valueOf(8)));
        String currentDateInUkraine = new Date().toString();
        String averageSalaryUA = NumberFormat.getCurrencyInstance(locale).format(10000);
        System.out.println(bundle.getString(String.valueOf(9)) + ": " + averageSalaryUA);
        System.out.println(bundle.getString(String.valueOf(10)) + ": " + currentDateInUkraine);
    }

    public void showEnglishInfo() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        show();
    }

    public void showUkraineInfo() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("Menu", locale);
        setMenu();
        show();
    }


    private void testRegEx() {
        String message = "Hello world";
        System.out.println(message);
        String message2 = message.replaceAll("o", "_");
        System.out.println(message2);
    }

    public void testStringUtils() {
        StringUtils utils = new StringUtils();
        utils.addToParameters("EPAM")
                .addToParameters("SYSTEMS");
        System.out.println(utils.concat());
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String key : menu.keySet()) {
            if (key.length() == 1) {
                System.out.println(menu.get(key));
            }
        }
    }


    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

    public static void main(String[] args) {
        App app = new App();
        app.show();

    }


}
